##Variância
A variância de uma variável aleatória ou processo estocástico é uma medida da sua dispersão estatística, indicando "o quão longe" em geral os seus valores se encontram do valor esperado.

##Variância Amostral
A variância amostral é uma medida de dispersão ou variabilidade dos dados, relativamente à medida de localização média.

##Variância Populacional
Variância populacional de uma variável de tipo quantitativo, é o valor médio dos quadrados dos desvios relativamente ao valor médio, dos dados que se obtêm quando se observa essa variável sobre todos os elementos da população, que assumimos finita.

##Coeficiente de Variação
O coeficiente de variação, também conhecido como desvio padrão relativo, é uma medida padronizada de dispersão de uma distribuição de probabilidade ou de uma distribuição de frequências.

##Desvio Padrão Populacional e Amostral
Desvio padrão ou desvio padrão populacional é uma medida de dispersão em torno da média populacional de uma variável aleatória. O termo possui também uma acepção específica no campo da estatística, na qual também é chamado de desvio padrão amostral e indica uma medida de dispersão dos dados em torno de média amostral. Um baixo desvio padrão indica que os pontos dos dados tendem a estar próximos da média ou do valor esperado. Um alto desvio padrão indica que os pontos dos dados estão espalhados por uma ampla gama de valores. O desvio padrão populacional ou amostral é a raiz quadrada da variância populacional ou amostral correspondente, de modo a ser uma medida de dispersão que seja um número não negativo e que use a mesma unidade de medida dos dados fornecidos.