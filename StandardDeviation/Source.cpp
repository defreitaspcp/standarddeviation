/*
Author: de Freitas, P.C.P
Description:
1 - Arithmetic Mean,
2 - Sample Variance,
3 - Population Variance,
4 - Sample Standard Deviation,
5 - Population Standard Deviation,
6 - Coefficient of Variation.
*/
#include<iostream>
#include<cmath>
using namespace std;
double mean(double data[], int length)
{
	double sum = 0;
	for (size_t i = 0; i < length; i++)
	{
		sum += data[i];
	}
	return sum / length;
}
double sampleVariance(double data[], int length)
{
	double sum = 0;
	for (size_t i = 0; i < length; i++)
	{
		sum += pow(data[i] - mean(data, length),2);
	}
	return sum / length - 1;
}
double populationVariance(double data[], int length)
{
	double sum = 0;
	for (size_t i = 0; i < length; i++)
	{
		sum += pow(data[i] - mean(data, length), 2);
	}
	return sum / length;
}
double sampleStandardDeviation(double data[], int length)
{
	return sqrt(sampleVariance(data, length));
}
double populationStandardDeviation(double data[], int length)
{
	return sqrt(populationVariance(data, length));
}
double coefficientVariation(double data[], int length)
{
	return populationStandardDeviation(data, length) / mean(data, length);
}
int main()
{
	double v[5] = { 600, 470, 170, 430, 300 };
	cout << "Mean : " << mean(v, 5) << endl;
	cout << "Sample Variance : " << sampleVariance(v, 5) << endl;
	cout << "Population Variance : " << populationVariance(v, 5) << endl;
	cout << "Coefficient of Variation" << coefficientVariation(v, 5) << endl;
	cout << "Sample Standard Deviation : " << sampleStandardDeviation(v, 5) << endl;
	cout << "Population Standard Deviation : "<< populationStandardDeviation(v, 5) << endl;
	return 0;
}